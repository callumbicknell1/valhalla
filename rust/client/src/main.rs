use std::process::Command;
use inquire::{
    error::{CustomUserError, InquireResult},
    required,
    ui::{Attributes, Color, RenderConfig, StyleSheet, Styled},
    CustomType, MultiSelect, Select, Text, InquireError,
};

fn build(handler_name: String, handler_type: &str) {
    let output = Command::new("make")
        .arg("agent")
        .arg("-name")
        .arg(handler_name)
        .arg("-type")
        .arg(handler_type)
        .output()
        .expect("Failed to execute command");

    println!("{0}", String::from_utf8_lossy(&output.stdout));
}

fn main() {
    let handler_name_ans = Text::new("Enter a handler name:").prompt();
    match handler_name_ans {
        Ok(_) => (),
        Err(_) => {
            println!("An error happened when asking for your name, try again later.");
            return;
        },
    }

    let handler_types: Vec<&str> = vec!["HTTP", "HTTPS", "DNS"];
    let ans: Result<&str, InquireError> = Select::new("Select a handler type:", handler_types).prompt();
    match ans {
        Ok(_choice) => (),
        Err(_) => println!("There was an error, please try again"),
    }

    let handler_name = handler_name_ans.unwrap();
    let handler_type = ans.unwrap();

    println!("Name: {0}", handler_name);
    println!("Type: {0}", handler_type);

    build(handler_name, handler_type);
}
